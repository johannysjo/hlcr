#include "raymarching.h"
#include "core.h"
#include "scene.h"

#include <glm/geometric.hpp>
#include <glm/mat4x4.hpp>
#include <glm/trigonometric.hpp>
#include <glm/vec4.hpp>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <stdint.h>

namespace hlcr {

static bool is_inside_volume(const glm::vec3 &local_pos)
{
    const float tol = 1e-6;
    return (-tol <= local_pos[0] && local_pos[0] <= 1.0f + tol && -tol <= local_pos[1] &&
            local_pos[1] <= 1.0f + tol && -tol <= local_pos[2] && local_pos[2] <= 1.0f + tol);
}

static glm::vec3 raymarch_visibility(const hlcr::Ray &world_ray, const hlcr::Volume &volume,
                                     const hlcr::Scene &scene, const float step_size_m,
                                     const float jitter)
{
    assert(world_ray.t_min >= 0.0f);
    assert(world_ray.t_max >= world_ray.t_min);
    assert(volume.type == hlcr::VOLUME_TYPE_VOXELS);
    assert(step_size_m > 0.0f);
    assert(jitter >= 0.0f);
    assert(volume.voxel_buffer_id < scene.voxel_buffers.size());

    // Here we assume that world_ray has been intersected against the volume bounding box, so that
    // t_min and t_max are the distances to, respectively, the closest and farthest intersection
    // points. The closest intersection point can be inside the bounding box.
    const float max_ray_length_m = std::fmax(0.0f, world_ray.t_max - world_ray.t_min);
    const uint32_t num_steps = uint32_t(std::ceil(max_ray_length_m / step_size_m));
    const float transmittance_threshold = 0.01f; // used for early ray termination
    const hlcr::VoxelBuffer &voxel_buffer = scene.voxel_buffers[volume.voxel_buffer_id];
    const hlcr::Material &material = scene.materials[volume.material_id];
    assert(material.type == hlcr::MATERIAL_TYPE_VOLUME);

    glm::vec3 transmittance = {1.0f, 1.0f, 1.0f};
    float t = world_ray.t_min + jitter * step_size_m;
    for (uint32_t i = 0; i < num_steps; ++i) {
        const glm::vec3 world_pos = world_ray.origin + t * world_ray.direction;
        const glm::vec3 local_pos = glm::vec3(volume.local_from_world * glm::vec4(world_pos, 1.0f));

        const float density =
            volume.density * hlcr::get_interp_voxel_density(voxel_buffer, local_pos);
        transmittance *=
            hlcr::beer_lambert(material.scattering, material.absorption, density, step_size_m);

        if (transmittance[0] <= transmittance_threshold &&
            transmittance[1] <= transmittance_threshold &&
            transmittance[2] <= transmittance_threshold) {
            transmittance = glm::vec3(0.0f, 0.0f, 0.0f);
            break;
        }

        t += step_size_m;
    }

    return transmittance;
}

static glm::vec3 get_visibility(const glm::vec3 &world_pos, const hlcr::PointLight &point_light,
                                const hlcr::Scene &scene, const float step_size_m,
                                const float jitter)
{
    assert(step_size_m > 0.0f);
    assert(jitter >= 0.0f);

    const glm::vec3 world_ray_dir = glm::normalize(point_light.position - world_pos);
    const float t_min = 1e-3f;
    const float t_max = glm::length(point_light.position - world_pos);

    glm::vec3 transmittance = {1.0f, 1.0f, 1.0f};

    // Evaluate visibility/transmittance for solid occluders. The visibility computation is
    // terminated at the first hit because all solid occluders are assumed to be opaque.
    for (const auto &sphere : scene.spheres) {
        const hlcr::Ray world_ray = {world_pos, world_ray_dir, t_min, t_max};
        hlcr::HitInfo hit_info;
        if (hlcr::sphere_hit(sphere, world_ray, hit_info)) {
            transmittance = glm::vec3(0.0f, 0.0f, 0.0f);
            return transmittance;
        }
    }

    // Evaluate visibility for volumetric occluders
    const hlcr::Box local_bbox = {glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f)};
    const float transmittance_threshold = 0.01f;
    for (const auto &volume : scene.volumes) {
        hlcr::Ray local_ray{};
        local_ray.origin = glm::vec3(volume.local_from_world * glm::vec4(world_pos, 1.0f));
        local_ray.direction = glm::mat3(volume.local_from_world) * world_ray_dir;
        local_ray.t_min = t_min;
        local_ray.t_max = t_max;

        hlcr::HitInfo hit_info{};
        if (hlcr::box_hit(local_bbox, local_ray, hit_info)) {
            if (volume.type == hlcr::VOLUME_TYPE_VOXELS) {
                const hlcr::Ray world_ray = {world_pos, world_ray_dir, hit_info.t_min,
                                             hit_info.t_max};
                transmittance *= raymarch_visibility(world_ray, volume, scene, step_size_m, jitter);
            }
            else if (volume.type == hlcr::VOLUME_TYPE_HOMOGENEOUS) {
                const hlcr::Material &material = scene.materials[volume.material_id];
                assert(material.type == hlcr::MATERIAL_TYPE_VOLUME);
                const float path_length_m = std::fmax(0.0f, hit_info.t_max - hit_info.t_min);
                transmittance *= hlcr::beer_lambert(material.scattering, material.absorption,
                                                    volume.density, path_length_m);
            }
            else {
                assert(false && "unsupported volume type");
            }

            // Terminate early if the accumulated transmittance falls below the specified
            // threshold. This speeds up the rendering of dense volumes.
            if (transmittance[0] <= transmittance_threshold &&
                transmittance[1] <= transmittance_threshold &&
                transmittance[2] <= transmittance_threshold) {
                transmittance = glm::vec3(0.0f, 0.0f, 0.0f);
                break;
            }
        }
    }

    return transmittance;
}

static void raymarch(const hlcr::Ray &world_ray, const hlcr::Volume &volume,
                     const hlcr::Scene &scene, const float step_size_m,
                     const float visibility_step_size_m, const float jitter,
                     glm::vec3 &transmittance, glm::vec3 &luminance)
{
    assert(world_ray.t_min >= 0.0f);
    assert(world_ray.t_max >= world_ray.t_min);
    assert(volume.material_id < scene.materials.size());
    assert(step_size_m > 0.0f);
    assert(jitter >= 0.0f);

    // Here we assume that world_ray has been intersected against the volume bounding box, so that
    // t_min and t_max are the distances to, respectively, the closest and farthest intersection
    // points. The closest intersection point can be inside the bounding box.
    const float max_ray_length_m = std::fmax(0.0f, world_ray.t_max - world_ray.t_min);
    const uint32_t num_steps = uint32_t(std::ceil(max_ray_length_m / step_size_m));
    const float transmittance_threshold = 0.01f; // used for early ray termination
    const hlcr::Material &material = scene.materials[volume.material_id];

    transmittance = {1.0f, 1.0f, 1.0f};
    luminance = {0.0f, 0.0f, 0.0f};
    float t = world_ray.t_min + jitter * step_size_m;
    for (uint32_t i = 0; i < num_steps; ++i) {
        const glm::vec3 world_pos = world_ray.origin + t * world_ray.direction;
        const glm::vec3 local_pos = glm::vec3(volume.local_from_world * glm::vec4(world_pos, 1.0f));
        if (!is_inside_volume(local_pos)) {
            break;
        }

        const float density =
            volume.type == hlcr::VOLUME_TYPE_VOXELS
                ? volume.density * hlcr::get_interp_voxel_density(
                                       scene.voxel_buffers[volume.voxel_buffer_id], local_pos)
                : volume.density;

        transmittance *=
            hlcr::beer_lambert(material.scattering, material.absorption, density, step_size_m);

        for (const auto &point_light : scene.point_lights) {
            const glm::vec3 visibility =
                get_visibility(world_pos, point_light, scene, visibility_step_size_m, 0.0f);

            const glm::vec3 world_pos_to_light = point_light.position - world_pos;
            const glm::vec3 L = glm::normalize(world_pos_to_light);
            const float attenuation = hlcr::get_point_light_attenuation(world_pos_to_light);

            luminance += transmittance * density * material.scattering * visibility *
                         hlcr::phase_function_hg(-L, -world_ray.direction, material.phase_g) *
                         attenuation * point_light.intensity * step_size_m;
        }
        luminance += transmittance * density * material.emission * step_size_m;

        if (transmittance[0] <= transmittance_threshold &&
            transmittance[1] <= transmittance_threshold &&
            transmittance[2] <= transmittance_threshold) {
            transmittance = glm::vec3(0.0f, 0.0f, 0.0f);
            break;
        }

        t += step_size_m;
    }
}

glm::vec3 trace_ray_rm(hlcr::Ray &world_ray, const hlcr::Scene &scene, const float step_size_m,
                       const float visibility_step_size_m, const float jitter)
{
    assert(world_ray.t_min >= 0.0f);
    assert(world_ray.t_max >= world_ray.t_min);
    assert(step_size_m > 0.0f);
    assert(jitter >= 0.0f);

    glm::vec3 luminance_accum = {0.0f, 0.0f, 0.0f};
    glm::vec3 transmittance_accum = {1.0f, 1.0f, 1.0f};

    for (const auto &sphere : scene.spheres) {
        hlcr::HitInfo hit_info{};
        if (hlcr::sphere_hit(sphere, world_ray, hit_info)) {
            const glm::vec3 world_pos = world_ray.origin + hit_info.t_min * world_ray.direction;
            const glm::vec3 N = glm::normalize(world_pos - sphere.center);
            const hlcr::Material &material = scene.materials[sphere.material_id];
            assert(material.type == hlcr::MATERIAL_TYPE_SOLID);

            luminance_accum = glm::vec3(0.0f, 0.0f, 0.0f);
            for (const auto &point_light : scene.point_lights) {
                const glm::vec3 visibility =
                    get_visibility(world_pos, point_light, scene, visibility_step_size_m, 0.0f);
                const glm::vec3 world_pos_to_light = point_light.position - world_pos;
                const glm::vec3 L = glm::normalize(world_pos_to_light);
                const float attenuation = hlcr::get_point_light_attenuation(world_pos_to_light);
                luminance_accum += visibility * material.base_color *
                                   std::fmax(0.0f, glm::dot(N, L)) * attenuation *
                                   point_light.intensity;
            }

            transmittance_accum = glm::vec3(0.0f, 0.0f, 0.0f);

            world_ray.t_max = hit_info.t_min;
        }
    }

    const hlcr::Box local_bbox = {glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f)};
    for (const auto &volume : scene.volumes) {
        hlcr::Ray local_ray{};
        local_ray.origin = glm::vec3(volume.local_from_world * glm::vec4(world_ray.origin, 1.0));
        local_ray.direction = glm::mat3(volume.local_from_world) * world_ray.direction;
        local_ray.t_min = world_ray.t_min;
        local_ray.t_max = world_ray.t_max;

        hlcr::HitInfo hit_info{};
        if (hlcr::box_hit(local_bbox, local_ray, hit_info)) {
            const hlcr::Ray world_ray_tmp = {world_ray.origin, world_ray.direction, hit_info.t_min,
                                             hit_info.t_max};

            glm::vec3 luminance = {0.0f, 0.0f, 0.0f};
            glm::vec3 transmittance = {1.0f, 1.0f, 1.0f};
            raymarch(world_ray_tmp, volume, scene, step_size_m, visibility_step_size_m, jitter,
                     transmittance, luminance);

            luminance_accum = transmittance * luminance_accum + luminance;
            transmittance_accum *= transmittance;
        }
    }

    return luminance_accum +
           transmittance_accum * hlcr::sample_sky_light(scene.sky_light, world_ray.direction);
}

} // namespace hlcr
