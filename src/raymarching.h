/// @file
/// @brief Raymarching backend.
///
/// @section LICENSE
///
/// Copyright (c) 2022 Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/vec3.hpp>

namespace hlcr {

struct Ray;
struct Scene;

glm::vec3 trace_ray_rm(hlcr::Ray &world_ray, const hlcr::Scene &scene, float step_size_m,
                       float visibility_step_size_m, float jitter);

} // namespace hlcr
