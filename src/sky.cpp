#include "sky.h"

#include <glm/common.hpp>
#include <glm/geometric.hpp>
#include <glm/vec2.hpp>
#include <omp.h>

#undef NDEBUG
#include <cassert>
#include <cmath>

namespace {

const float PI = 3.141592653589793f;

struct Ray {
    glm::vec3 origin;
    glm::vec3 direction;
    float t_min;
    float t_max;
};

struct HitInfo {
    float t_min;
    float t_max;
};

// R2 low-discrepancy sequence
// Reference: http://extremelearning.com.au/unreasonable-effectiveness-of-quasirandom-sequences/
glm::vec2 ldseq_r2(const uint32_t index)
{
    const float phi = 1.324717957244746f;
    const float x = std::fmod(index / phi, 1.0f);
    const float y = std::fmod(index / (phi * phi), 1.0f);

    return {x, y};
}

// Converts a 2D equirectangular texture coordinate to a 3D ray direction
glm::vec3 generate_ray_dir_equirect(const glm::vec2 &uv)
{
    const glm::vec2 uv_clamped = glm::clamp(uv, 0.0f, 1.0f);

    const float theta = PI * (2.0 * uv_clamped.s - 1.0);
    const float phi = PI * (uv_clamped.t - 0.5);
    const float x = std::cos(phi) * std::sin(theta);
    const float y = std::sin(phi);
    const float z = std::cos(phi) * std::cos(theta);

    return glm::normalize(glm::vec3(x, y, z));
}

// Ray-sphere intersection test that finds the distance to the closest (t_min) and farthest (t_max)
// intersection points between a ray and a solid sphere. Returns true if the ray hits the sphere
// and false otherwise.
bool sphere_hit(const glm::vec3 &sphere_center, const float sphere_radius, const Ray &ray,
                HitInfo &hit_info)
{
    const glm::vec3 oc = ray.origin - sphere_center;
    const float a = glm::dot(ray.direction, ray.direction);
    const float b = glm::dot(oc, ray.direction);
    const float c = glm::dot(oc, oc) - sphere_radius * sphere_radius;

    const float discriminant = b * b - a * c;
    if (discriminant > 0.0f) {
        hit_info.t_min = std::fmax(ray.t_min, (-b - std::sqrt(discriminant)) / a);
        hit_info.t_max = (-b + std::sqrt(discriminant)) / a;
        return hit_info.t_max > ray.t_min && hit_info.t_max < ray.t_max;
    }

    return false;
}

float rayleigh_phase_function(const float cos_theta)
{
    return 0.75 * (1.0 + cos_theta * cos_theta);
}

float mie_phase_function(const float cos_theta, const float g)
{
    const float g2 = g * g;
    return 1.5f * ((1.0f - g2) * (1.0f + cos_theta * cos_theta)) /
           ((2.0f + g2) * std::pow(1.0f + g2 - 2.0f * g * cos_theta, 1.5f));
}

glm::vec3 beer_lambert(const glm::vec3 sigma_t, const float path_length_km)
{
    return glm::exp(-sigma_t * path_length_km);
}

// NOTE: visibility is the same as transmittance in this case
glm::vec3 raymarch_visibility(const glm::vec3 &world_pos, const uint32_t num_steps,
                              const hlcr::SkyParams &sky_params)
{
    Ray world_ray;
    world_ray.origin = world_pos;
    world_ray.direction = -sky_params.sun_direction;
    world_ray.t_min = 1e-3f;
    world_ray.t_max = 1e6f;

    glm::vec3 visibility = glm::vec3(1.0f, 1.0f, 1.0f);
    HitInfo hit_info;
    if (sphere_hit(sky_params.earth_center, sky_params.earth_radius_km, world_ray, hit_info)) {
        visibility = glm::vec3(0.0f, 0.0f, 0.0f);
    }
    else if (sphere_hit(sky_params.earth_center, sky_params.atmosphere_radius_km, world_ray,
                        hit_info)) {
        world_ray.t_min = hit_info.t_min;
        world_ray.t_max = hit_info.t_max;

        const float path_length_km = world_ray.t_max - world_ray.t_min;
        const float step_length_km = path_length_km / float(num_steps);

        float t = world_ray.t_min;
        while (t < world_ray.t_max) {
            const glm::vec3 sample_pos =
                world_ray.origin + (t + 0.5f * step_length_km) * world_ray.direction;

            const float height_km = std::fmax(0.0f, length(sample_pos - sky_params.earth_center) -
                                                        sky_params.earth_radius_km);

            const float rayleigh_scaling =
                std::exp(-height_km / sky_params.rayleigh_scale_height_km);
            visibility *=
                beer_lambert(sky_params.rayleigh_scattering * rayleigh_scaling, step_length_km);

            const float mie_scaling = std::exp(-height_km / sky_params.mie_scale_height_km);
            visibility *= beer_lambert(sky_params.mie_scattering * mie_scaling, step_length_km);

            t += step_length_km;
        }
    }

    return visibility;
}

void raymarch(const Ray &world_ray, const uint32_t num_primary_steps,
              const uint32_t num_secondary_steps, const hlcr::SkyParams &sky_params,
              glm::vec3 &transmittance, glm::vec3 &luminance)
{
    const float path_length_km = world_ray.t_max - world_ray.t_min;
    const float step_length_km = path_length_km / float(num_primary_steps);

    transmittance = glm::vec3(1.0f, 1.0f, 1.0f);
    luminance = glm::vec3(0.0f, 0.0f, 0.0f);
    float t = world_ray.t_min;
    while (t < world_ray.t_max) {
        const glm::vec3 sample_pos =
            world_ray.origin + (t + 0.5f * step_length_km) * world_ray.direction;

        const float height_km = std::fmax(0.0f, length(sample_pos - sky_params.earth_center) -
                                                    sky_params.earth_radius_km);

        const glm::vec3 visibility =
            raymarch_visibility(sample_pos, num_secondary_steps, sky_params);

        const float cos_theta = glm::dot(sky_params.sun_direction, -world_ray.direction);

        // Evaluate transmittance
        const float rayleigh_scaling = std::exp(-height_km / sky_params.rayleigh_scale_height_km);
        transmittance *=
            beer_lambert(sky_params.rayleigh_scattering * rayleigh_scaling, step_length_km);

        const float mie_scaling = std::exp(-height_km / sky_params.mie_scale_height_km);
        transmittance *= beer_lambert(sky_params.mie_scattering * mie_scaling, step_length_km);

        // Evaluate luminance
        luminance += transmittance * sky_params.rayleigh_scattering * rayleigh_scaling *
                     visibility * rayleigh_phase_function(cos_theta) * sky_params.sun_intensity *
                     step_length_km;

        luminance += transmittance * sky_params.mie_scattering * mie_scaling * visibility *
                     mie_phase_function(cos_theta, sky_params.mie_phase_g) *
                     sky_params.sun_intensity * step_length_km;

        t += step_length_km;
    }
}

} // namespace

namespace hlcr {

SkyParams get_default_sky_params(const glm::vec3 &sun_direction, const glm::vec3 &sun_intensity)
{
    // NOTE: the Mie and Rayleigh scattering parameter values used here are based on the values
    // used by Bruneton and Neyret in the paper Precomputed Atmospheric Scattering, EGSR, 2008
    SkyParams sky_params;
    sky_params.earth_center = glm::vec3(0.0f, 0.0f, 0.0f);
    sky_params.earth_radius_km = 6360.0f;
    sky_params.atmosphere_radius_km = 6420.0f;
    sky_params.mie_scattering = glm::vec3(3.0e-3f);
    sky_params.rayleigh_scattering = glm::vec3(4.8e-3f, 1.35e-2f, 3.31e-2f);
    sky_params.mie_scale_height_km = 1.2f;
    sky_params.rayleigh_scale_height_km = 8.0f;
    sky_params.mie_phase_g = 0.76f;
    sky_params.sun_direction = glm::normalize(sun_direction);
    sky_params.sun_intensity = sun_intensity;

    return sky_params;
}

// Generates an RGBA32F equirectangular HDR environment map image of the sky by simulating
// atmospheric Mie and Rayleigh scattering with raymarching. The number of samples per pixel (spp)
// can be increased to, e.g, 4 or 8 to produce anti-aliasing. Increasing the num_primary_steps
// and/or num_secondary_steps will make the raymarching more accurate but also slower.
//
// The implementation largely follows the approach of the Scratchapixel tutorial
// https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/simulating-sky/
// simulating-colors-of-the-sky.
void create_rgba32f_sky_envmap(const SkyParams &sky_params, const float altitude_km,
                               const uint32_t width, const uint32_t height, const uint32_t spp,
                               const uint32_t num_primary_steps, const uint32_t num_secondary_steps,
                               std::vector<float> &pixel_data)
{
    assert(altitude_km >= 0.0f);
    assert(width > 0);
    assert(height > 0);
    assert(spp > 0);
    assert(spp < 32);
    assert(num_primary_steps > 0);
    assert(num_primary_steps < 1000);
    assert(num_secondary_steps > 0);
    assert(num_secondary_steps < 100);

    const uint32_t num_channels = 4;

    pixel_data.resize(width * height * num_channels);
#pragma omp parallel for schedule(dynamic, 1)
    for (uint32_t j = 0; j < height; ++j) {
        for (uint32_t i = 0; i < width; ++i) {
            const glm::vec2 uv = {float(i) / width, 1.0f - float(j) / height};

            glm::vec3 color = {0.0f, 0.0f, 0.0f};
            for (uint32_t k = 1; k <= spp; ++k) {
                const glm::vec2 aa_offset = ldseq_r2(k) / glm::vec2(width, height);

                Ray world_ray;
                world_ray.origin = glm::vec3(0.0f, sky_params.earth_radius_km + altitude_km, 0.0f);
                world_ray.direction = generate_ray_dir_equirect(uv + aa_offset);
                world_ray.t_min = 1e-3f;
                world_ray.t_max = 1e6f;

                HitInfo hit_info;
                if (sphere_hit(sky_params.earth_center, sky_params.atmosphere_radius_km, world_ray,
                               hit_info)) {
                    world_ray.t_min = hit_info.t_min;
                    world_ray.t_max = hit_info.t_max;

                    // Check if the ray intersects the earth surface and clamp t_max to the
                    // intersection point if that is the case (no need to raymarch beneath the
                    // surface, won't reach any sunlight there)
                    if (sphere_hit(sky_params.earth_center, sky_params.earth_radius_km, world_ray,
                                   hit_info)) {
                        world_ray.t_max = std::fmax(hit_info.t_min, world_ray.t_min);
                    }

                    // Calculate atmospheric scattering with raymarching
                    glm::vec3 transmittance = {1.0f, 1.0f, 1.0f};
                    glm::vec3 luminance = {0.0f, 0.0f, 1.0f};
                    raymarch(world_ray, num_primary_steps, num_secondary_steps, sky_params,
                             transmittance, luminance);

                    color += luminance;
                }
            }
            color /= float(spp);

            // color[0] = uv[0];
            // color[1] = uv[1];
            // color[2] = 0.0f;

            const uint32_t pixel_index = (i + j * width) * num_channels;
            pixel_data[pixel_index + 0] = color[0];
            pixel_data[pixel_index + 1] = color[1];
            pixel_data[pixel_index + 2] = color[2];
            pixel_data[pixel_index + 3] = 1.0f;
        }
    }
}

} // namespace hlcr
