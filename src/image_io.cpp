#include "image_io.h"

#define STBI_ONLY_HDR
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#undef NDEBUG
#include <cassert>
#include <cstdio>

namespace hlcr {

// Portable PixMap (PPM) image writer
// Reference: http://netpbm.sourceforge.net/doc/ppm.html
void write_ppm(const char *filename, const std::vector<float> &image, const uint32_t width,
               const uint32_t height, const uint32_t num_channels)
{
    assert(image.size() == width * height * num_channels);
    assert(num_channels >= 3);

    std::FILE *fp = std::fopen(filename, "w");
    if (fp == nullptr) {
        return;
    }

    // Write header
    std::fprintf(fp, "P3\n");
    std::fprintf(fp, "%u %u\n", width, height);
    std::fprintf(fp, "255\n");

    // Write image data. PPM stores pixels in left to right, top to bottom order.
    for (uint32_t j = 0; j < height; ++j) {
        for (uint32_t i = 0; i < width; ++i) {
            const uint32_t pixel_index = (i + j * width) * num_channels;
            const auto r = uint32_t(255.999f * image[pixel_index + 0]);
            const auto g = uint32_t(255.999f * image[pixel_index + 1]);
            const auto b = uint32_t(255.999f * image[pixel_index + 2]);
            std::fprintf(fp, "%u %u %u ", r, g, b);
        }
        std::fprintf(fp, "\n");
    }

    std::fclose(fp);
}

// Portable FloatMap (PFM) image writer
// Reference: http://www.pauldebevec.com/Research/HDR/PFM/
void write_pfm(const char *filename, const std::vector<float> &image, const uint32_t width,
               const uint32_t height, const uint32_t num_channels)
{
    assert(image.size() == width * height * num_channels);
    assert(num_channels >= 3);

    std::FILE *fp = std::fopen(filename, "wb");
    if (fp == nullptr) {
        return;
    }

    // Write header
    std::fprintf(fp, "PF\n");
    std::fprintf(fp, "%u %u\n", width, height);
    std::fprintf(fp, "-1.0\n"); // indicates little-endian byte order

    // Write image data. PFM stores pixels in left to right, bottom to top order.
    for (uint32_t j = 0; j < height; ++j) {
        for (uint32_t i = 0; i < width; ++i) {
            const uint32_t pixel_index = (i + (height - 1 - j) * width) * num_channels;
            std::fwrite(&image[pixel_index], 1, num_channels * sizeof(float), fp);
        }
    }

    std::fclose(fp);
}

void load_hdr_image(const char *filename, std::vector<float> &pixels, uint32_t &width,
                    uint32_t &height, uint32_t &num_channels)
{
    assert(pixels.empty());

    int32_t width_i32, height_i32, num_channels_in_file;
    const int32_t desired_num_channels = 4;
    float *data =
        stbi_loadf(filename, &width_i32, &height_i32, &num_channels_in_file, desired_num_channels);
    assert(data != nullptr);
    assert(width_i32 > 0);
    assert(height_i32 > 0);
    assert(num_channels_in_file > 0);

    width = uint32_t(width_i32);
    height = uint32_t(height_i32);
    num_channels = uint32_t(desired_num_channels);
    const uint32_t num_pixel_values = width * height * num_channels;
    pixels.reserve(num_pixel_values);
    for (uint32_t i = 0; i < num_pixel_values; ++i) {
        pixels.push_back(data[i]);
    }
}

} // namespace hlcr
