#include "procedural.h"

#include <glm/common.hpp>
#include <glm/geometric.hpp>
#include <glm/vec2.hpp>
#include <omp.h>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>

namespace {

inline float aastep(const float edge, const float aa_width, const float x)
{
    return glm::smoothstep(edge - aa_width, edge + aa_width, x);
}

inline float hash_2d(const glm::vec2 &seed)
{
    return glm::fract(std::sin(glm::dot(seed, glm::vec2(12.9898f, 78.233f))) * 43758.5453f);
}

inline float hash_3d(const glm::vec3 &seed)
{
    return hash_2d(glm::vec2(hash_2d(glm::vec2(seed[0], seed[1])), seed[2]));
}

// Generates 3D value noise by interpolating values from a simple 3D hash (white noise) function.
// The scale of the noise can be changed by scaling the input position, and the pattern can be
// changed by using using different seed values in the range [0,1).
float value_noise_3d(const glm::vec3 &pos, const float seed)
{
    const glm::vec3 grid_pos = glm::floor(pos);
    const glm::vec3 d = pos - grid_pos;

    const float v000 = hash_3d(grid_pos + glm::vec3(0.0f, 0.0f, 0.0f) + seed);
    const float v100 = hash_3d(grid_pos + glm::vec3(1.0f, 0.0f, 0.0f) + seed);
    const float v010 = hash_3d(grid_pos + glm::vec3(0.0f, 1.0f, 0.0f) + seed);
    const float v110 = hash_3d(grid_pos + glm::vec3(1.0f, 1.0f, 0.0f) + seed);
    const float v001 = hash_3d(grid_pos + glm::vec3(0.0f, 0.0f, 1.0f) + seed);
    const float v101 = hash_3d(grid_pos + glm::vec3(1.0f, 0.0f, 1.0f) + seed);
    const float v011 = hash_3d(grid_pos + glm::vec3(0.0f, 1.0f, 1.0f) + seed);
    const float v111 = hash_3d(grid_pos + glm::vec3(1.0f, 1.0f, 1.0f) + seed);

    const float v00 = v000 * (1.0f - d[0]) + v100 * d[0];
    const float v01 = v001 * (1.0f - d[0]) + v101 * d[0];
    const float v10 = v010 * (1.0f - d[0]) + v110 * d[0];
    const float v11 = v011 * (1.0f - d[0]) + v111 * d[0];

    const float v0 = v00 * (1.0f - d[1]) + v10 * d[1];
    const float v1 = v01 * (1.0f - d[1]) + v11 * d[1];

    return v0 * (1.0f - d[2]) + v1 * d[2];
}

// 3D fractal Brownian motion (fBm) noise generator
float fbm_3d(const glm::vec3 &pos, const uint32_t num_octaves, const float seed)
{
    const float lacunarity = 1.92;
    const float gain = 0.5f;
    float scale = 1.0f;
    float amplitude = 1.0f;
    float value = 0.0f;
    for (uint32_t i = 0; i < num_octaves; ++i) {
        value += amplitude * value_noise_3d(scale * pos, seed);
        scale *= lacunarity;
        amplitude *= gain;
    }
    value *= (1.0 - gain);
    value = std::fmax(0.0f, std::fmin(1.0f, value));

    return value;
}

inline uint8_t get_voxel_value(const std::vector<uint8_t> &voxels, const glm::uvec3 &dimensions,
                               const glm::vec3 &voxel_pos)
{
    if (voxel_pos[0] < 0.0f || voxel_pos[0] >= dimensions[0] || voxel_pos[1] < 0.0f ||
        voxel_pos[1] >= dimensions[1] || voxel_pos[2] < 0.0f || voxel_pos[2] >= dimensions[2]) {
        return 0;
    }

    const uint64_t i = uint64_t(std::floor(voxel_pos[0]));
    const uint64_t j = uint64_t(std::floor(voxel_pos[1]));
    const uint64_t k = uint64_t(std::floor(voxel_pos[2]));
    const uint64_t voxel_index = i + j * dimensions[0] + k * dimensions[0] * dimensions[1];

    return voxels[voxel_index];
}

inline uint8_t get_voxel_value(const std::vector<uint8_t> &voxels, const glm::uvec3 &dimensions,
                               const glm::ivec3 &voxel_pos)
{
    if (voxel_pos[0] < 0 || voxel_pos[0] >= dimensions[0] || voxel_pos[1] < 0 ||
        voxel_pos[1] >= dimensions[1] || voxel_pos[2] < 0 || voxel_pos[2] >= dimensions[2]) {
        return 0;
    }

    const uint64_t voxel_index = voxel_pos[0] + voxel_pos[1] * dimensions[0] +
                                 uint64_t(voxel_pos[2] * dimensions[0] * dimensions[1]);

    return voxels[voxel_index];
}

inline uint8_t get_interp_voxel_value(const std::vector<uint8_t> &voxels,
                                      const glm::uvec3 &dimensions, const glm::vec3 &voxel_pos)
{
    const glm::ivec3 ivoxel_pos = glm::ivec3(glm::floor(voxel_pos));
    const float xd = voxel_pos[0] - ivoxel_pos[0];
    const float yd = voxel_pos[1] - ivoxel_pos[1];
    const float zd = voxel_pos[2] - ivoxel_pos[2];

    const auto v000 = float(get_voxel_value(voxels, dimensions, ivoxel_pos));
    const auto v100 = float(get_voxel_value(voxels, dimensions, ivoxel_pos + glm::ivec3(1, 0, 0)));
    const auto v110 = float(get_voxel_value(voxels, dimensions, ivoxel_pos + glm::ivec3(1, 1, 0)));
    const auto v010 = float(get_voxel_value(voxels, dimensions, ivoxel_pos + glm::ivec3(0, 1, 0)));
    const auto v001 = float(get_voxel_value(voxels, dimensions, ivoxel_pos + glm::ivec3(0, 0, 1)));
    const auto v101 = float(get_voxel_value(voxels, dimensions, ivoxel_pos + glm::ivec3(1, 0, 1)));
    const auto v111 = float(get_voxel_value(voxels, dimensions, ivoxel_pos + glm::ivec3(1, 1, 1)));
    const auto v011 = float(get_voxel_value(voxels, dimensions, ivoxel_pos + glm::ivec3(0, 1, 1)));

    const float v00 = v000 * (1.0f - xd) + v100 * xd;
    const float v01 = v001 * (1.0f - xd) + v101 * xd;
    const float v10 = v010 * (1.0f - xd) + v110 * xd;
    const float v11 = v011 * (1.0f - xd) + v111 * xd;

    const float v0 = v00 * (1.0f - yd) + v10 * yd;
    const float v1 = v01 * (1.0f - yd) + v11 * yd;
    const float v = v0 * (1.0f - zd) + v1 * zd;

    return uint8_t(std::fmin(v * 256.0f, 255.0f));
}

} // namespace

namespace hlcr {

// Creates a dummy voxel buffer with an anti-aliased sphere. Currently assumes that the volume is
// cubic with isotropic voxels.
void create_dummy_voxel_buffer(const glm::uvec3 &dimensions, std::vector<uint8_t> &voxels)
{
    assert(dimensions[0] > 0);
    assert(dimensions[1] > 0);
    assert(dimensions[2] > 0);

    const float aa_width = 0.7f / dimensions[0]; // XXX
    const float sphere_radius = std::fmax(0.0f, 0.5f - aa_width);
    const glm::vec3 sphere_center = glm::vec3(0.5f, 0.5f, 0.5f);

    voxels.reserve(uint64_t(dimensions[0]) * dimensions[1] * dimensions[2]);
    for (uint32_t k = 0; k < dimensions[2]; ++k) {
        for (uint32_t j = 0; j < dimensions[1]; ++j) {
            for (uint32_t i = 0; i < dimensions[0]; ++i) {
                const glm::uvec3 voxel_pos = {i, j, k};

                // TODO: calculate the distance in world or voxel coordinates instead of local
                // [0,1] coordinates
                const glm::vec3 local_pos = glm::vec3(voxel_pos) / glm::vec3(dimensions);
                const float dist = glm::length(local_pos - sphere_center);
                const float density = 1.0f - aastep(sphere_radius, aa_width, dist);

                const uint8_t voxel_value = uint8_t(std::floor(density * 255.0f));
                voxels.push_back(voxel_value);
            }
        }
    }
}

void cloudify_voxel_buffer(const std::vector<uint8_t> &voxels_in, const glm::uvec3 &dimensions,
                           const float perturbation_in_voxels, std::vector<uint8_t> &voxels_out)
{
    assert(!voxels_in.empty());
    assert(uint64_t(dimensions[0]) * dimensions[1] * dimensions[2] == voxels_in.size());
    assert(perturbation_in_voxels >= 0.0f);

    voxels_out.resize(uint64_t(dimensions[0]) * dimensions[1] * dimensions[2], 0);
    #pragma omp parallel for schedule(dynamic, 1)
    for (uint32_t k = 0; k < dimensions[2]; ++k) {
        for (uint32_t j = 0; j < dimensions[1]; ++j) {
            for (uint32_t i = 0; i < dimensions[0]; ++i) {
                const glm::vec3 voxel_pos = {float(i), float(j), float(k)};

                const glm::vec3 noise_rgb = {fbm_3d(0.1f * voxel_pos, 3, 0.0f),
                                             fbm_3d(0.1f * voxel_pos, 3, 0.1f),
                                             fbm_3d(0.1f * voxel_pos, 3, 0.2f)};

                const glm::vec3 perturbed_voxel_pos =
                    voxel_pos + perturbation_in_voxels * (2.0f * noise_rgb - 1.0f);
                const uint8_t voxel_value =
                    get_voxel_value(voxels_in, dimensions, perturbed_voxel_pos);

                const uint64_t voxel_index =
                    i + j * dimensions[0] + uint64_t(k) * dimensions[0] * dimensions[1];
                voxels_out[voxel_index] = voxel_value;
            }
        }
    }
}

} // namespace hlcr
