#include "volume_io.h"

#undef NDEBUG
#include <cassert>
#include <cstdio>
#include <cstring>

namespace hlcr {

bool load_uint8_vtk_volume(const std::string &filename, std::vector<uint8_t> &voxels,
                           glm::uvec3 &dimensions, glm::vec3 &spacing)
{
    std::FILE *fp = std::fopen(filename.c_str(), "rb");
    if (fp == nullptr) {
        std::fclose(fp);
        return false;
    }

    // Read header
    const uint32_t num_header_lines = 10;
    const uint32_t max_line_length = 256;
    char lines[num_header_lines][max_line_length];
    for (uint32_t i = 0; i < num_header_lines; ++i) {
        if (std::fgets(lines[i], max_line_length, fp) == nullptr) {
            std::fclose(fp);
            return false;
        }
    }

    if (!(std::strncmp(lines[0], "# vtk", 5) == 0 || std::strncmp(lines[0], "# VTK", 5) == 0) ||
        std::strncmp(lines[2], "BINARY", 6) != 0 ||
        std::strncmp(lines[3], "DATASET STRUCTURED_POINTS", 25) != 0) {
        std::fclose(fp);
        return false;
    }

    size_t num_voxels = 0;
    char data_type[256];
    for (uint32_t i = 4; i < num_header_lines; ++i) {
        if (std::strncmp(lines[i], "DIMENSIONS", 10) == 0) {
            std::sscanf(lines[i], "%*s %u %u %u", &dimensions[0], &dimensions[1], &dimensions[2]);
        }
        else if (std::strncmp(lines[i], "SPACING", 7) == 0) {
            std::sscanf(lines[i], "%*s %f %f %f", &spacing[0], &spacing[1], &spacing[2]);
        }
        else if (std::strncmp(lines[i], "POINT_DATA", 10) == 0) {
            std::sscanf(lines[i], "%*s %lu", &num_voxels);
        }
        else if (std::strncmp(lines[i], "SCALARS", 7) == 0) {
            std::sscanf(lines[i], "%*s %*s %s", data_type);
        }
    }
    assert(num_voxels > 0);
    assert(size_t(dimensions[0]) * dimensions[1] * dimensions[2] == num_voxels);
    if (std::strncmp(data_type, "unsigned_char", 5) != 0) {
        std::fclose(fp);
        return false;
    }

    // Read data
    voxels.resize(num_voxels);
    const size_t result = std::fread(voxels.data(), sizeof(uint8_t), num_voxels, fp);
    std::fclose(fp);
    if (result != num_voxels) {
        return false;
    }

    return true;
}

} // namespace hlcr
