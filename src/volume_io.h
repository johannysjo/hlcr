/// @file
/// @brief Volume image IO functions.
///
/// @section LICENSE
///
/// Copyright (c) 2020 Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/vec3.hpp>

#include <stdint.h>
#include <string>
#include <vector>

namespace hlcr {

bool load_uint8_vtk_volume(const std::string &filename, std::vector<uint8_t> &voxels,
                           glm::uvec3 &dimensions, glm::vec3 &spacing);

} // namespace hlcr
