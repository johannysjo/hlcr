#include "delta_tracking.h"
#include "core.h"
#include "scene.h"

#include <glm/geometric.hpp>
#include <glm/mat4x4.hpp>
#include <glm/trigonometric.hpp>
#include <glm/vec4.hpp>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <stdint.h>

namespace hlcr {

static bool is_inside_volume(const glm::vec3 &local_pos)
{
    const float tol = 1e-6;
    return (-tol <= local_pos[0] && local_pos[0] <= 1.0f + tol && -tol <= local_pos[1] &&
            local_pos[1] <= 1.0f + tol && -tol <= local_pos[2] && local_pos[2] <= 1.0f + tol);
}

// Samples a heterogeneous or homogeneous volume by delta (Woodcock) tracking. Returns true if the
// ray hits a particle in the volume and false otherwise.
static bool delta_tracking(const hlcr::Ray &world_ray, const hlcr::Volume &volume,
                           const hlcr::Scene &scene, hlcr::RNGState &rng_state,
                           const uint32_t spectrum_index, float &t)
{
    assert(world_ray.t_min >= 0.0f);
    assert(world_ray.t_max >= world_ray.t_min);
    assert(volume.material_id < scene.materials.size());
    assert(spectrum_index <= 2);

    const hlcr::Material &material = scene.materials[volume.material_id];
    const float max_attenuation_coeff =
        hlcr::get_attenuation_coeff(material)[spectrum_index]; // given in inverse meters, m^-1

    t = world_ray.t_min;
    bool collision = false;
    while (t < world_ray.t_max) {
        const float step_size_m =
            -std::log(std::fmax(1e-4, hlcr::randf(rng_state))) / max_attenuation_coeff;
        t += step_size_m;

        const glm::vec3 world_pos = world_ray.origin + t * world_ray.direction;
        const glm::vec3 local_pos = glm::vec3(volume.local_from_world * glm::vec4(world_pos, 1.0f));
        if (!is_inside_volume(local_pos)) {
            break;
        }

        const float density =
            volume.type == hlcr::VOLUME_TYPE_VOXELS
                ? volume.density * hlcr::get_interp_voxel_density(
                                       scene.voxel_buffers[volume.voxel_buffer_id], local_pos)
                : volume.density;

        if (hlcr::randf(rng_state) < density) {
            collision = true;
            break;
        }
    }

    return collision;
}

static glm::vec3 get_visibility(const glm::vec3 &world_pos, const hlcr::PointLight &point_light,
                                const hlcr::Scene &scene, hlcr::RNGState &rng_state,
                                const uint32_t spectrum_index)
{
    const glm::vec3 world_ray_dir = glm::normalize(point_light.position - world_pos);
    const float t_min = 1e-3f;
    const float t_max = glm::length(point_light.position - world_pos);

    glm::vec3 transmittance = {1.0f, 1.0f, 1.0f};

    // Evaluate visibility/transmittance for solid occluders. The visibility computation is
    // terminated at the first hit because all solid occluders are assumed to be opaque.
    for (const auto &sphere : scene.spheres) {
        const hlcr::Ray world_ray = {world_pos, world_ray_dir, t_min, t_max};
        hlcr::HitInfo hit_info{};
        if (hlcr::sphere_hit(sphere, world_ray, hit_info)) {
            transmittance = glm::vec3(0.0f, 0.0f, 0.0f);
            return transmittance;
        }
    }

    // Evaluate visibility for volumetric occluders
    const hlcr::Box local_bbox = {glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f)};
    for (const auto &volume : scene.volumes) {
        hlcr::Ray local_ray{};
        local_ray.origin = glm::vec3(volume.local_from_world * glm::vec4(world_pos, 1.0f));
        local_ray.direction = glm::mat3(volume.local_from_world) * world_ray_dir;
        local_ray.t_min = t_min;
        local_ray.t_max = t_max;

        hlcr::HitInfo hit_info{};
        if (hlcr::box_hit(local_bbox, local_ray, hit_info)) {
            if (volume.type == hlcr::VOLUME_TYPE_VOXELS) {
                const hlcr::Ray world_ray = {world_pos, world_ray_dir, hit_info.t_min,
                                             hit_info.t_max};
                float t = 0.0f;
                if (delta_tracking(world_ray, volume, scene, rng_state, spectrum_index, t)) {
                    transmittance = glm::vec3(0.0f, 0.0f, 0.0f);
                    return transmittance;
                }
            }
            else {
                assert(false && "unsupported volume type");
            }
        }
    }

    return transmittance;
}

static glm::vec3 get_visibility(const glm::vec3 &world_pos, const glm::vec3 &world_ray_dir,
                                const float t_max, const hlcr::Scene &scene,
                                hlcr::RNGState &rng_state, const uint32_t spectrum_index)
{
    const float t_min = 1e-3f;
    glm::vec3 transmittance = {1.0f, 1.0f, 1.0f};

    // Evaluate visibility/transmittance for solid occluders. The visibility computation is
    // terminated at the first hit because all solid occluders are assumed to be opaque.
    for (const auto &sphere : scene.spheres) {
        const hlcr::Ray world_ray = {world_pos, world_ray_dir, t_min, t_max};
        hlcr::HitInfo hit_info{};
        if (hlcr::sphere_hit(sphere, world_ray, hit_info)) {
            transmittance = glm::vec3(0.0f, 0.0f, 0.0f);
            return transmittance;
        }
    }

    // Evaluate visibility for volumetric occluders
    const hlcr::Box local_bbox = {glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f)};
    for (const auto &volume : scene.volumes) {
        hlcr::Ray local_ray{};
        local_ray.origin = glm::vec3(volume.local_from_world * glm::vec4(world_pos, 1.0f));
        local_ray.direction = glm::mat3(volume.local_from_world) * world_ray_dir;
        local_ray.t_min = t_min;
        local_ray.t_max = t_max;

        hlcr::HitInfo hit_info{};
        if (hlcr::box_hit(local_bbox, local_ray, hit_info)) {
            if (volume.type == hlcr::VOLUME_TYPE_VOXELS) {
                const hlcr::Ray world_ray = {world_pos, world_ray_dir, hit_info.t_min,
                                             hit_info.t_max};
                float t = 0.0f;
                if (delta_tracking(world_ray, volume, scene, rng_state, spectrum_index, t)) {
                    transmittance = glm::vec3(0.0f, 0.0f, 0.0f);
                    return transmittance;
                }
            }
            else {
                assert(false && "unsupported volume type");
            }
        }
    }

    return transmittance;
}

#if 1
glm::vec3 trace_ray_dt(hlcr::Ray &world_ray, const hlcr::Scene &scene, hlcr::RNGState &rng_state)
{
    assert(world_ray.t_min >= 0.0f);
    assert(world_ray.t_max >= world_ray.t_min);

    // Volumetric attenuation coefficients can be spectrum varying, so for delta tracking we need to
    // randomly select one of the RGB color channels and track the luminance and transmittance of
    // that channel separately. The final color value is weighted by the probability density
    // function (PDF) of selecting the channel to compensate for this.
    const uint32_t spectrum_index = uint32_t(std::floor(2.999f * hlcr::randf(rng_state)));
    const float pdf = 1.0f / 3.0f;

    glm::vec3 luminance_accum = {0.0f, 0.0f, 0.0f};
    glm::vec3 transmittance_accum = {1.0f, 1.0f, 1.0f};

// Evaluate luminance and transmittance for solid objects
#if 1
    for (const auto &sphere : scene.spheres) {
        hlcr::HitInfo hit_info{};
        if (hlcr::sphere_hit(sphere, world_ray, hit_info)) {
            const glm::vec3 world_pos = world_ray.origin + hit_info.t_min * world_ray.direction;
            const glm::vec3 N = glm::normalize(world_pos - sphere.center);
            const glm::vec3 offset = 1e-3f * N;
            const hlcr::Material &material = scene.materials[sphere.material_id];
            assert(material.type == hlcr::MATERIAL_TYPE_SOLID);

            // Point lights
            luminance_accum = glm::vec3(0.0f, 0.0f, 0.0f);
            for (const auto &point_light : scene.point_lights) {
                const glm::vec3 world_pos_to_light = point_light.position - world_pos;
                const glm::vec3 L = glm::normalize(world_pos_to_light);
                const float t_max = glm::length(world_pos_to_light);
                const glm::vec3 visibility =
                    get_visibility(world_pos + offset, L, t_max, scene, rng_state, spectrum_index);
                const float attenuation = hlcr::get_point_light_attenuation(world_pos_to_light);
                luminance_accum += visibility * material.base_color *
                                   std::fmax(0.0f, glm::dot(N, L)) * attenuation *
                                   point_light.intensity;
            }

            // Sky light
            {
                const float rnd_brdf_x = hlcr::randf(rng_state);
                const float rnd_brdf_y = hlcr::randf(rng_state);
                const glm::vec3 rnd_dir = hlcr::sample_sphere(rnd_brdf_x, rnd_brdf_y);
                const glm::vec3 scatter_dir = glm::normalize(N + 0.999f * rnd_dir);
                const glm::vec3 visibility = get_visibility(world_pos + offset, scatter_dir, 1e6,
                                                            scene, rng_state, spectrum_index);
                luminance_accum += visibility * material.base_color *
                                   hlcr::sample_sky_light(scene.sky_light, scatter_dir);
            }

            transmittance_accum = glm::vec3(0.0f, 0.0f, 0.0f);

            world_ray.t_max = hit_info.t_min;
        }
    }
#endif

    // Evaluate luminance and transmittance for volumetric objects
    // FIXME: the delta tracked result gets too bright, find out why
    const hlcr::Box local_bbox = {glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f)};
    for (const auto &volume : scene.volumes) {
        hlcr::Ray local_ray{};
        local_ray.origin = glm::vec3(volume.local_from_world * glm::vec4(world_ray.origin, 1.0));
        local_ray.direction = glm::mat3(volume.local_from_world) * world_ray.direction;
        local_ray.t_min = world_ray.t_min;
        local_ray.t_max = world_ray.t_max;

        hlcr::HitInfo hit_info{};
        if (hlcr::box_hit(local_bbox, local_ray, hit_info)) {
            const hlcr::Ray world_ray_tmp = {world_ray.origin, world_ray.direction, hit_info.t_min,
                                             hit_info.t_max};

            glm::vec3 luminance = {0.0f, 0.0f, 0.0f};
            glm::vec3 transmittance = {1.0f, 1.0f, 1.0f};
            float t = 0.0f;
            if (delta_tracking(world_ray_tmp, volume, scene, rng_state, spectrum_index, t)) {
                transmittance = glm::vec3(0.0f, 0.0f, 0.0f);
                const glm::vec3 world_pos = world_ray_tmp.origin + t * world_ray_tmp.direction;
                const hlcr::Material &material = scene.materials[volume.material_id];

                // Point lights
                for (const auto &point_light : scene.point_lights) {
                    const glm::vec3 world_pos_to_light = point_light.position - world_pos;
                    const glm::vec3 L = glm::normalize(world_pos_to_light);
                    const float t_max = glm::length(world_pos_to_light);
                    const float attenuation = hlcr::get_point_light_attenuation(world_pos_to_light);

                    const glm::vec3 visibility =
                        get_visibility(world_pos, L, t_max, scene, rng_state, spectrum_index);

                    luminance +=
                        visibility * material.scattering *
                        hlcr::phase_function_hg(-L, -world_ray_tmp.direction, material.phase_g) *
                        attenuation * point_light.intensity;
                }

                // Sky light
                {
                    const float rnd_brdf_x = hlcr::randf(rng_state);
                    const float rnd_brdf_y = hlcr::randf(rng_state);
                    const glm::vec3 L = glm::normalize(hlcr::sample_sphere(rnd_brdf_x, rnd_brdf_y));
                    const float t_max = 1e6f;
                    const glm::vec3 visibility =
                        get_visibility(world_pos, L, t_max, scene, rng_state, spectrum_index);
                    luminance +=
                        visibility * material.scattering *
                        hlcr::phase_function_hg(-L, -world_ray_tmp.direction, material.phase_g) *
                        hlcr::sample_sky_light(scene.sky_light, L);
                }

                luminance += material.emission;
            }

            luminance_accum = transmittance * luminance_accum + luminance;
            transmittance_accum *= transmittance;
        }
    }

    glm::vec3 output_color = {0.0f, 0.0f, 0.0f};
    output_color[spectrum_index] =
        (luminance_accum[spectrum_index] +
         transmittance_accum[spectrum_index] *
             hlcr::sample_sky_light(scene.sky_light, world_ray.direction)[spectrum_index]) /
        pdf;

    return output_color;
}
#else
glm::vec3 trace_ray_dt(hlcr::Ray &world_ray, const hlcr::Scene &scene, hlcr::RNGState &rng_state)
{
    assert(world_ray.t_min >= 0.0f);
    assert(world_ray.t_max >= world_ray.t_min);

    // Volumetric attenuation coefficients can be spectrum varying, so for delta tracking we need to
    // randomly select one of the RGB color channels and track the luminance and transmittance of
    // that channel separately. The final color value is weighted by the probability density
    // function (PDF) of selecting the channel to compensate for this.
    const uint32_t spectrum_index = uint32_t(std::floor(2.999f * hlcr::randf(rng_state)));
    const float pdf = 1.0f / 3.0f;

    glm::vec3 luminance_accum = {0.0f, 0.0f, 0.0f};
    glm::vec3 transmittance_accum = {1.0f, 1.0f, 1.0f};

// Evaluate luminance and transmittance for solid objects
#if 1
    for (const auto &sphere : scene.spheres) {
        hlcr::HitInfo hit_info{};
        if (hlcr::sphere_hit(sphere, world_ray, hit_info)) {
            const glm::vec3 world_pos = world_ray.origin + hit_info.t_min * world_ray.direction;
            const glm::vec3 N = glm::normalize(world_pos - sphere.center);
            const hlcr::Material &material = scene.materials[sphere.material_id];
            assert(material.type == hlcr::MATERIAL_TYPE_SOLID);

            luminance_accum = glm::vec3(0.0f, 0.0f, 0.0f);
            for (const auto &point_light : scene.point_lights) {
                const glm::vec3 visibility =
                    get_visibility(world_pos, point_light, scene, rng_state, spectrum_index);
                const glm::vec3 world_pos_to_light = point_light.position - world_pos;
                const glm::vec3 L = glm::normalize(world_pos_to_light);
                const float attenuation = hlcr::get_point_light_attenuation(world_pos_to_light);
                luminance_accum += visibility * material.base_color *
                                   std::fmax(0.0f, glm::dot(N, L)) * attenuation *
                                   point_light.intensity;
            }

            {
                const float rnd_brdf_x = hlcr::randf(rng_state);
                const float rnd_brdf_y = hlcr::randf(rng_state);
                const glm::vec3 rnd_dir = hlcr::sample_sphere(rnd_brdf_x, rnd_brdf_y);
                const glm::vec3 scatter_dir = glm::normalize(N + 0.999f * rnd_dir);
                const glm::vec3 visibility = {1.0f, 1.0f, 1.0f};
                luminance_accum += visibility * material.base_color *
                                   hlcr::sample_sky_light(scene.sky_light, scatter_dir);
            }

            transmittance_accum = glm::vec3(0.0f, 0.0f, 0.0f);

            world_ray.t_max = hit_info.t_min;
        }
    }
#endif

    // Evaluate luminance and transmittance for volumetric objects
    // FIXME: the delta tracked result gets too bright, find out why
    const hlcr::Box local_bbox = {glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f)};
    for (const auto &volume : scene.volumes) {
        hlcr::Ray local_ray{};
        local_ray.origin = glm::vec3(volume.local_from_world * glm::vec4(world_ray.origin, 1.0));
        local_ray.direction = glm::mat3(volume.local_from_world) * world_ray.direction;
        local_ray.t_min = world_ray.t_min;
        local_ray.t_max = world_ray.t_max;

        hlcr::HitInfo hit_info{};
        if (hlcr::box_hit(local_bbox, local_ray, hit_info)) {
            const hlcr::Ray world_ray_tmp = {world_ray.origin, world_ray.direction, hit_info.t_min,
                                             hit_info.t_max};

            glm::vec3 luminance = {0.0f, 0.0f, 0.0f};
            glm::vec3 transmittance = {1.0f, 1.0f, 1.0f};
            float t = 0.0f;
            if (delta_tracking(world_ray_tmp, volume, scene, rng_state, spectrum_index, t)) {
                transmittance = glm::vec3(0.0f, 0.0f, 0.0f);

                const glm::vec3 world_pos = world_ray_tmp.origin + t * world_ray_tmp.direction;
                const hlcr::Material &material = scene.materials[volume.material_id];
                for (const auto &point_light : scene.point_lights) {
                    const glm::vec3 visibility =
                        get_visibility(world_pos, point_light, scene, rng_state, spectrum_index);

                    const glm::vec3 world_pos_to_light = point_light.position - world_pos;
                    const glm::vec3 L = glm::normalize(world_pos_to_light);
                    const float attenuation = hlcr::get_point_light_attenuation(world_pos_to_light);

                    luminance +=
                        visibility * material.scattering *
                        hlcr::phase_function_hg(-L, -world_ray_tmp.direction, material.phase_g) *
                        attenuation * point_light.intensity;
                }

                {
                    const float rnd_brdf_x = hlcr::randf(rng_state);
                    const float rnd_brdf_y = hlcr::randf(rng_state);
                    const glm::vec3 L = glm::normalize(hlcr::sample_sphere(rnd_brdf_x, rnd_brdf_y));
                    const glm::vec3 visibility = {1.0f, 1.0f, 1.0f};
                    luminance +=
                        visibility * material.scattering *
                        hlcr::phase_function_hg(-L, -world_ray_tmp.direction, material.phase_g) *
                        hlcr::sample_sky_light(scene.sky_light, L);
                }

                luminance += material.emission;
            }

            luminance_accum = transmittance * luminance_accum + luminance;
            transmittance_accum *= transmittance;
        }
    }

    glm::vec3 output_color = {0.0f, 0.0f, 0.0f};
    output_color[spectrum_index] =
        (luminance_accum[spectrum_index] +
         transmittance_accum[spectrum_index] *
             hlcr::sample_sky_light(scene.sky_light, world_ray.direction)[spectrum_index]) /
        pdf;

    return output_color;
}
#endif

} // namespace hlcr
