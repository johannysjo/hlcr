#include "core.h"

#include <glm/common.hpp>
#include <glm/trigonometric.hpp>

#undef NDEBUG
#include <cassert>
#include <cmath>

namespace {

const float PI = 3.141592653589793f;

} // namespace

namespace hlcr {

float randf(RNGState &rng_state)
{
    rng_state.seed = uint32_t(1664525) * rng_state.seed + uint32_t(1013904223);
    return float(rng_state.seed) / 4294967296.0f;
}

// Reference: https://www.reedbeta.com/blog/quick-and-easy-gpu-random-numbers-in-d3d11/
uint32_t wang_hash(uint32_t seed)
{
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);

    return seed;
}

glm::vec2 ldseq_r2(const uint32_t index)
{
    const float phi = 1.324717957244746f;
    const float x = std::fmod(index / phi, 1.0f);
    const float y = std::fmod(index / (phi * phi), 1.0f);

    return {x, y};
}

float hash2d(const float x, const float y)
{
    return std::fmod(std::abs(std::sin(12.9898f * x + 78.233f * y)) * 43758.5453f, 1.0f);
}

glm::vec3 sample_sphere(const float u, const float v)
{
    const float z = 2.0f * u - 1.0f;
    const float phi = 2.0f * PI * v;
    const float r = std::sqrt(1.0f - z * z);
    const float x = r * std::cos(phi);
    const float y = r * std::sin(phi);

    return {x, y, z};
}

void generate_ray(const Camera &camera, const glm::vec2 &uv, Ray &ray)
{
    const glm::vec3 f = glm::normalize(camera.center - camera.eye);
    const glm::vec3 s = glm::cross(f, glm::normalize(camera.up));
    const glm::vec3 u = glm::cross(s, f);

    const float half_height = std::tan(0.5f * camera.fovy);
    const float half_width = camera.aspect * half_height;
    const float x = 2.0f * uv[0] - 1.0f;
    const float y = 2.0f * uv[1] - 1.0f;

    ray.origin = camera.eye;
    ray.direction = glm::normalize(f + x * half_width * s + y * half_height * u);
}

glm::vec3 get_attenuation_coeff(const Material &material)
{
    return material.scattering + material.absorption;
}

bool box_hit(const Box &box, const Ray &ray, HitInfo &hit_info)
{
    const glm::vec3 ray_dir_inv = 1.0f / ray.direction;
    const glm::vec3 t0 = (box.min_corner - ray.origin) * ray_dir_inv;
    const glm::vec3 t1 = (box.max_corner - ray.origin) * ray_dir_inv;
    const glm::vec3 t_min3 = glm::min(t0, t1);
    const glm::vec3 t_max3 = glm::max(t0, t1);
    const float t_min =
        glm::clamp(std::fmax(t_min3[0], std::fmax(t_min3[1], t_min3[2])), ray.t_min, ray.t_max);
    const float t_max =
        glm::clamp(std::fmin(t_max3[0], std::fmin(t_max3[1], t_max3[2])), ray.t_min, ray.t_max);
    hit_info.t_min = t_min;
    hit_info.t_max = t_max;

    return t_max >= t_min;
}

bool sphere_hit(const Sphere &sphere, const Ray &ray, HitInfo &hit_info)
{
    const glm::vec3 oc = ray.origin - sphere.center;
    const float a = glm::dot(ray.direction, ray.direction);
    const float b = glm::dot(oc, ray.direction);
    const float c = glm::dot(oc, oc) - sphere.radius * sphere.radius;

    const float discriminant = b * b - a * c;
    if (discriminant > 0.0f) {
        hit_info.t_min = std::fmax(ray.t_min, (-b - std::sqrt(discriminant)) / a);
        hit_info.t_max = (-b + std::sqrt(discriminant)) / a;
        return hit_info.t_max > ray.t_min && hit_info.t_max < ray.t_max;
    }

    return false;
}

float get_voxel_density(const VoxelBuffer &voxel_buffer, const glm::uvec3 &voxel_pos)
{
    if (voxel_pos[0] >= voxel_buffer.dimensions[0] || voxel_pos[1] >= voxel_buffer.dimensions[1] ||
        voxel_pos[2] >= voxel_buffer.dimensions[2]) {
        return 0.0f;
    }

    const uint64_t voxel_index =
        voxel_pos[0] + voxel_pos[1] * voxel_buffer.dimensions[0] +
        uint64_t(voxel_pos[2]) * voxel_buffer.dimensions[0] * voxel_buffer.dimensions[1];
    const float density = float(voxel_buffer.voxels[voxel_index]) / 255.0f;

    return density;
}

float get_voxel_density(const VoxelBuffer &voxel_buffer, const glm::vec3 &local_pos)
{
    if (local_pos[0] < 0.0f || local_pos[0] >= 1.0f || local_pos[1] < 0.0f ||
        local_pos[1] >= 1.0f || local_pos[2] < 0.0f || local_pos[2] >= 1.0f) {
        return 0.0f;
    }

    const glm::uvec3 voxel_pos = {uint32_t(std::floor(local_pos[0] * voxel_buffer.dimensions[0])),
                                  uint32_t(std::floor(local_pos[1] * voxel_buffer.dimensions[1])),
                                  uint32_t(std::floor(local_pos[2] * voxel_buffer.dimensions[2]))};
    const float density = get_voxel_density(voxel_buffer, voxel_pos);

    return density;
}

float get_interp_voxel_density(const VoxelBuffer &voxel_buffer, const glm::vec3 &local_pos)
{
    if (local_pos[0] < 0.0f || local_pos[0] >= 1.0f || local_pos[1] < 0.0f ||
        local_pos[1] >= 1.0f || local_pos[2] < 0.0f || local_pos[2] >= 1.0f) {
        return 0.0f;
    }

    const glm::uvec3 voxel_pos = {uint32_t(std::floor(local_pos[0] * voxel_buffer.dimensions[0])),
                                  uint32_t(std::floor(local_pos[1] * voxel_buffer.dimensions[1])),
                                  uint32_t(std::floor(local_pos[2] * voxel_buffer.dimensions[2]))};

    const float xd = std::fmod(local_pos[0] * voxel_buffer.dimensions[0], 1.0f);
    const float yd = std::fmod(local_pos[1] * voxel_buffer.dimensions[1], 1.0f);
    const float zd = std::fmod(local_pos[2] * voxel_buffer.dimensions[2], 1.0f);

    const float c000 = get_voxel_density(voxel_buffer, voxel_pos);
    const float c100 = get_voxel_density(voxel_buffer, voxel_pos + glm::uvec3(1, 0, 0));
    const float c110 = get_voxel_density(voxel_buffer, voxel_pos + glm::uvec3(1, 1, 0));
    const float c010 = get_voxel_density(voxel_buffer, voxel_pos + glm::uvec3(0, 1, 0));
    const float c001 = get_voxel_density(voxel_buffer, voxel_pos + glm::uvec3(0, 0, 1));
    const float c101 = get_voxel_density(voxel_buffer, voxel_pos + glm::uvec3(1, 0, 1));
    const float c111 = get_voxel_density(voxel_buffer, voxel_pos + glm::uvec3(1, 1, 1));
    const float c011 = get_voxel_density(voxel_buffer, voxel_pos + glm::uvec3(0, 1, 1));

    const float c00 = c000 * (1.0f - xd) + c100 * xd;
    const float c01 = c001 * (1.0f - xd) + c101 * xd;
    const float c10 = c010 * (1.0f - xd) + c110 * xd;
    const float c11 = c011 * (1.0f - xd) + c111 * xd;

    const float c0 = c00 * (1.0f - yd) + c10 * yd;
    const float c1 = c01 * (1.0f - yd) + c11 * yd;
    const float interp_density = c0 * (1.0f - zd) + c1 * zd;

    return interp_density;
}

static glm::vec4 get_rgba32f_pixel_value(const PixelBuffer &pixel_buffer,
                                         const glm::uvec2 &pixel_pos)
{
    assert(pixel_buffer.width > 0);
    assert(pixel_buffer.height > 0);
    assert(pixel_buffer.num_channels == 4);
    assert(pixel_buffer.width * pixel_buffer.height * pixel_buffer.num_channels ==
           pixel_buffer.pixels.size());

    const uint32_t i = pixel_pos[0] < pixel_buffer.width ? pixel_pos[0] : pixel_buffer.width - 1;
    const uint32_t j = pixel_pos[1] < pixel_buffer.height ? pixel_pos[1] : pixel_buffer.height - 1;
    const uint32_t pixel_index = (i + j * pixel_buffer.width) * pixel_buffer.num_channels;

    const float r = pixel_buffer.pixels[pixel_index];
    const float g = pixel_buffer.pixels[pixel_index + 1];
    const float b = pixel_buffer.pixels[pixel_index + 2];
    const float a = pixel_buffer.pixels[pixel_index + 3];

    return {r, g, b, a};
}

static glm::vec4 get_interp_rgba32f_pixel_value(const PixelBuffer &pixel_buffer,
                                                const glm::vec2 &local_pos)
{
    assert(pixel_buffer.width > 0);
    assert(pixel_buffer.height > 0);
    assert(pixel_buffer.num_channels == 4);

    const glm::vec2 local_pos_01 = glm::clamp(local_pos, 0.0f, 1.0f - 1e-6f);
    const glm::uvec2 pixel_pos = {std::floor(local_pos_01[0] * pixel_buffer.width),
                                  std::floor(local_pos_01[1] * pixel_buffer.height)};

    const float xd = std::fmod(local_pos_01[0] * pixel_buffer.width, 1.0f);
    const float yd = std::fmod(local_pos_01[1] * pixel_buffer.height, 1.0f);

    const glm::vec4 c00 = get_rgba32f_pixel_value(pixel_buffer, pixel_pos);
    const glm::vec4 c01 = get_rgba32f_pixel_value(pixel_buffer, pixel_pos + glm::uvec2(0, 1));
    const glm::vec4 c10 = get_rgba32f_pixel_value(pixel_buffer, pixel_pos + glm::uvec2(1, 0));
    const glm::vec4 c11 = get_rgba32f_pixel_value(pixel_buffer, pixel_pos + glm::uvec2(1, 1));

    const glm::vec4 c0 = c00 * (1.0f - xd) + c10 * xd;
    const glm::vec4 c1 = c01 * (1.0f - xd) + c11 * xd;
    const glm::vec4 interp_color = c0 * (1.0f - yd) + c1 * yd;

    return interp_color;
}

glm::vec4 sample_rgba32f_envmap(const PixelBuffer &pixel_buffer, const glm::vec3 &ray_dir)
{
    assert(pixel_buffer.width > 0);
    assert(pixel_buffer.height > 0);
    assert(pixel_buffer.num_channels == 4);

    // Convert the 3D ray direction to a 2D equirectangular texture coordinate
    const float phi = std::asin(ray_dir[1]);
    const float theta = std::atan2(ray_dir[0], ray_dir[2]);
    const float s = 0.5f * theta / PI + 0.5f;
    const float t = phi / PI + 0.5f;

    // Sample the environment map
    // XXX: using 1.0 - t because the sampled image appears upside down otherwise
    const glm::vec2 local_pos = {s, 1.0f - t};
    const glm::vec4 color = get_interp_rgba32f_pixel_value(pixel_buffer, local_pos);

    return color;
}

float get_point_light_attenuation(const glm::vec3 &pos_to_light)
{
    return 1.0f / (glm::dot(pos_to_light, pos_to_light) + 1.0f);
}

glm::vec3 sample_sky_light(const SkyLight &sky_light, const glm::vec3 &world_ray_dir)
{
    glm::vec3 color = {0.0f, 0.0f, 0.0f};
    if (sky_light.type == SKY_LIGHT_TYPE_SOLID) {
        color = sky_light.intensity;
    }
    else if (sky_light.type == SKY_LIGHT_TYPE_ENVMAP) {
        color = glm::vec3(sample_rgba32f_envmap(sky_light.envmap, world_ray_dir));
    }
    else {
        assert(false && "Unknown sky light type");
    }

    return color;
}

glm::vec3 beer_lambert(const glm::vec3 &scattering, const glm::vec3 &absorption,
                       const float density, const float path_length_m)
{
    const glm::vec3 optical_thickness = density * (scattering + absorption) * path_length_m;
    const glm::vec3 transmittance = glm::exp(-optical_thickness);

    return transmittance;
}

// The Henyey-Greenstein phase function. Given an incoming light direction w_in and an asymmetry
// factor g in the range [-1.0, 1.0], the function returns the fraction of light that is scattered
// in the direction w_out. g < 0.0 produces backward scattering, g = 0.0 isotropic scattering, and
// g > 0.0 forward scattering.
float phase_function_hg(const glm::vec3 &w_in, const glm::vec3 &w_out, const float g)
{
    return (1.0f - g * g) / std::pow((1.0f + g * g - 2.0f * g * glm::dot(w_in, w_out)), 1.5f);
}

// The double Henyey-Greenstein phase function. The function takes two asymmetry factors g1 and g2
// as input, computes the HG phase function for each factor, and blends the resulting values using
// linear interpolation and a blend parameter alpha in the range [0, 1].
float phase_function_double_hg(const glm::vec3 &w_in, const glm::vec3 &w_out, const float g1,
                               const float g2, const float alpha)
{
    return glm::mix(phase_function_hg(w_in, w_out, g1), phase_function_hg(w_in, w_out, g2), alpha);
}

glm::vec3 lin2srgb(const glm::vec3 &color)
{
    return glm::pow(color, glm::vec3(0.454f));
}

glm::vec3 tonemap_aces(const glm::vec3 &color)
{
    const float a = 2.51f;
    const float b = 0.03f;
    const float c = 2.43f;
    const float d = 0.59f;
    const float e = 0.14f;

    return glm::clamp((color * (a * color + b)) / (color * (c * color + d) + e), 0.0f, 1.0f);
}

} // namespace hlcr
