/// @file
/// @brief Procedural volume generation functions.
///
/// @section LICENSE
///
/// Copyright (c) 2020 Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace hlcr {

void create_dummy_voxel_buffer(const glm::uvec3 &dimensions, std::vector<uint8_t> &voxels);

void cloudify_voxel_buffer(const std::vector<uint8_t> &voxels_in, const glm::uvec3 &dimensions,
                           float perturbation_in_voxels, std::vector<uint8_t> &voxels_out);

} // namespace hlcr
