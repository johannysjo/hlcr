#pragma once

#include "core.h"

namespace hlcr {

struct Scene {
    std::vector<Volume> volumes{};
    std::vector<VoxelBuffer> voxel_buffers{};
    std::vector<Sphere> spheres{};
    std::vector<Material> materials{};
    std::vector<PointLight> point_lights{};
    SkyLight sky_light{};
};

} // namespace hlcr
