/// @file
/// @brief Core rendering functions, structs, and enums.
///
/// @section LICENSE
///
/// Copyright (c) 2020 Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace hlcr {

struct RNGState {
    uint32_t seed;
};

float randf(RNGState &rng_state);

uint32_t wang_hash(uint32_t seed);

glm::vec2 ldseq_r2(uint32_t index);

float hash2d(float x, float y);

glm::vec3 sample_sphere(float u, float v);

struct Ray {
    glm::vec3 origin;
    glm::vec3 direction;
    float t_min;
    float t_max;
};

struct HitInfo {
    float t_min;
    float t_max;
};

struct Camera {
    glm::vec3 eye;
    glm::vec3 center;
    glm::vec3 up;
    float fovy;
    float aspect;
};

void generate_ray(const Camera &camera, const glm::vec2 &uv, Ray &ray);

enum MaterialType { MATERIAL_TYPE_SOLID, MATERIAL_TYPE_VOLUME };

struct Material {
    MaterialType type;
    glm::vec3 base_color;
    glm::vec3 scattering;
    glm::vec3 absorption;
    glm::vec3 emission;
    float phase_g;
};

glm::vec3 get_attenuation_coeff(const Material &material);

struct Box {
    glm::vec3 min_corner;
    glm::vec3 max_corner;
};

bool box_hit(const Box &box, const Ray &ray, HitInfo &hit_info);

struct Sphere {
    glm::vec3 center;
    float radius;
    uint32_t material_id;
};

bool sphere_hit(const Sphere &sphere, const Ray &ray, HitInfo &hit_info);

enum VolumeType { VOLUME_TYPE_HOMOGENEOUS, VOLUME_TYPE_VOXELS };

struct Volume {
    VolumeType type;
    uint32_t material_id;
    uint32_t voxel_buffer_id;
    float density;
    glm::mat4 world_from_local;
    glm::mat4 local_from_world;
};

struct VoxelBuffer {
    std::vector<uint8_t> voxels;
    glm::uvec3 dimensions;
};

float get_voxel_density(const VoxelBuffer &voxel_buffer, const glm::uvec3 &voxel_pos);

float get_voxel_density(const VoxelBuffer &voxel_buffer, const glm::vec3 &local_pos);

float get_interp_voxel_density(const VoxelBuffer &voxel_buffer, const glm::vec3 &local_pos);

struct PixelBuffer {
    std::vector<float> pixels;
    uint32_t width;
    uint32_t height;
    uint32_t num_channels;
};

glm::vec4 sample_rgba32f_envmap(const PixelBuffer &pixel_buffer, const glm::vec3 &ray_dir);

struct PointLight {
    glm::vec3 position;
    glm::vec3 intensity;
};

float get_point_light_attenuation(const glm::vec3 &pos_to_light);

enum SkyLightType { SKY_LIGHT_TYPE_SOLID, SKY_LIGHT_TYPE_ENVMAP, SKY_LIGHT_TYPE_NONE };

struct SkyLight {
    SkyLightType type;
    PixelBuffer envmap;
    glm::vec3 intensity;
};

glm::vec3 sample_sky_light(const SkyLight &sky_light, const glm::vec3 &world_ray_dir);

glm::vec3 beer_lambert(const glm::vec3 &scattering, const glm::vec3 &absorption, float density,
                       float path_length_m);

float phase_function_hg(const glm::vec3 &w_in, const glm::vec3 &w_out, float g);

float phase_function_double_hg(const glm::vec3 &w_in, const glm::vec3 &w_out, float g1, float g2,
                               float alpha);

glm::vec3 lin2srgb(const glm::vec3 &color);

glm::vec3 tonemap_aces(const glm::vec3 &color);

} // namespace hlcr
