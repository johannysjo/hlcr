/// @file
/// @brief 2D image IO functions.
///
/// @section LICENSE
///
/// Copyright (c) 2020 Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <stdint.h>
#include <vector>

namespace hlcr {

void write_ppm(const char *filename, const std::vector<float> &image, uint32_t width,
               uint32_t height, uint32_t num_channels);

void write_pfm(const char *filename, const std::vector<float> &image, uint32_t width,
               uint32_t height, uint32_t num_channels);

void load_hdr_image(const char *filename, std::vector<float> &pixels, uint32_t &width,
                    uint32_t &height, uint32_t &num_channels);

} // namespace hlcr
