/// @file
/// @brief Delta tracking backend.
///
/// @section LICENSE
///
/// Copyright (c) 2022 Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/vec3.hpp>

namespace hlcr {

struct Ray;
struct Scene;
struct RNGState;

glm::vec3 trace_ray_dt(hlcr::Ray &world_ray, const hlcr::Scene &scene, hlcr::RNGState &rng_state);

} // namespace hlcr
