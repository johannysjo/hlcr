/// @file
/// @brief Volumetric path tracer.
///
/// @section LICENSE
///
/// Copyright (c) 2020 Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#include "core.h"
#include "delta_tracking.h"
#include "image_io.h"
#include "logging.h"
#include "procedural.h"
#include "raymarching.h"
#include "scene.h"
#include "sky.h"
#include "volume_io.h"

#include <glm/common.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/geometric.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <omp.h>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <stdint.h>
#include <string>
#include <vector>

enum SceneID { SCENE_ID_BUNNIES, SCENE_ID_DISNEY_CLOUD };

enum RenderMode { RENDER_MODE_RAYMARCHING, RENDER_MODE_DELTA_TRACKING };

struct Settings {
    SceneID scene_id = SCENE_ID_BUNNIES;
    uint32_t width = 400;
    uint32_t height = 200;
    uint32_t spp = 16;
    RenderMode render_mode = RENDER_MODE_RAYMARCHING;
    float step_size_m = 0.025f;           // only used for raymarching
    float visibility_step_size_m = 0.05f; // only used for raymarching
    float exposure = 60.0f;
};

void initialize_bunnies_scene(hlcr::Scene &scene)
{
    // Create a large sphere representing the ground
    hlcr::Sphere sphere{};
    sphere.center = glm::vec3(0.0f, -1000.0f, 0.0f);
    sphere.radius = 1000.0f;
    sphere.material_id = 3;
    scene.spheres.push_back(sphere);

    // Load and cloudify voxelized mesh
    {
        const std::string filename = "./data/bunny_256_opacity_uint8.vtk";
        hlcr::VoxelBuffer voxel_buffer;
        glm::vec3 spacing;
        if (!hlcr::load_uint8_vtk_volume(filename, voxel_buffer.voxels, voxel_buffer.dimensions,
                                         spacing)) {
            assert(false && "could not load VTK volume");
        }

        std::vector<uint8_t> voxels;
        const float perturbation_in_voxels = 20.0f;
        hlcr::cloudify_voxel_buffer(voxel_buffer.voxels, voxel_buffer.dimensions,
                                    perturbation_in_voxels, voxels);
        voxel_buffer.voxels = voxels;
        scene.voxel_buffers.push_back(voxel_buffer);
    }

    // Create volume instances
    {
        hlcr::Volume volume;
        volume.type = hlcr::VOLUME_TYPE_VOXELS;
        volume.material_id = 0;
        volume.voxel_buffer_id = 0;
        volume.density = 1.5f;
        const glm::vec3 volume_extent_m = {2.5f, 2.5f, 2.5f};
        volume.world_from_local =
            glm::translate(glm::mat4(1.0f), glm::vec3(-3.0f, 0.5f * volume_extent_m[1], 0.0f)) *
            glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::scale(glm::mat4(1.0f), volume_extent_m) *
            glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.5f, -0.5f));
        volume.local_from_world = glm::inverse(volume.world_from_local);
        scene.volumes.push_back(volume);
    }

    {
        hlcr::Volume volume;
        volume.type = hlcr::VOLUME_TYPE_VOXELS;
        volume.material_id = 1;
        volume.voxel_buffer_id = 0;
        volume.density = 1.5f;
        const glm::vec3 volume_extent_m = {2.5f, 2.5f, 2.5f};
        volume.world_from_local =
            glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.5f * volume_extent_m[1], 0.0f)) *
            glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::scale(glm::mat4(1.0f), volume_extent_m) *
            glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.5f, -0.5f));
        volume.local_from_world = glm::inverse(volume.world_from_local);
        scene.volumes.push_back(volume);
    }

    {
        hlcr::Volume volume;
        volume.type = hlcr::VOLUME_TYPE_VOXELS;
        volume.material_id = 2;
        volume.voxel_buffer_id = 0;
        volume.density = 1.5f;
        const glm::vec3 volume_extent_m = {2.5f, 2.5f, 2.5f};
        volume.world_from_local =
            glm::translate(glm::mat4(1.0f), glm::vec3(3.0f, 0.5f * volume_extent_m[1], 0.0f)) *
            glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::scale(glm::mat4(1.0f), volume_extent_m) *
            glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.5f, -0.5f));
        volume.local_from_world = glm::inverse(volume.world_from_local);
        scene.volumes.push_back(volume);
    }

    // Create materials
    {
        hlcr::Material material{};
        material.type = hlcr::MATERIAL_TYPE_VOLUME;
        material.base_color = glm::vec3(0.0f, 0.0f, 0.0f);
        material.scattering = glm::vec3(1.0f, 1.0f, 1.0f);
        material.absorption = glm::vec3(1.0f, 3.0f, 7.0f);
        material.emission = glm::vec3(0.0f, 0.0f, 0.0f);
        material.phase_g = 0.5f;
        scene.materials.push_back(material);
    }

    {
        hlcr::Material material{};
        material.type = hlcr::MATERIAL_TYPE_VOLUME;
        material.base_color = glm::vec3(0.0f, 0.0f, 0.0f);
        material.scattering = glm::vec3(1.0f, 1.0f, 1.0f);
        material.absorption = glm::vec3(3.0f, 1.0f, 7.0f);
        material.emission = glm::vec3(0.0f, 0.0f, 0.0f);
        material.phase_g = 0.5f;
        scene.materials.push_back(material);
    }

    {
        hlcr::Material material{};
        material.type = hlcr::MATERIAL_TYPE_VOLUME;
        material.base_color = glm::vec3(0.0f, 0.0f, 0.0f);
        material.scattering = glm::vec3(1.0f, 1.0f, 1.0f);
        material.absorption = glm::vec3(7.0f, 3.0f, 1.0f);
        material.emission = glm::vec3(0.0f, 0.0f, 0.0f);
        material.phase_g = 0.5f;
        scene.materials.push_back(material);
    }

    {
        hlcr::Material material{};
        material.type = hlcr::MATERIAL_TYPE_SOLID;
        material.base_color = glm::vec3(0.2f, 0.2f, 0.2f);
        material.scattering = glm::vec3(0.0f, 0.0f, 0.0f);
        material.absorption = glm::vec3(0.0f, 0.0f, 0.0f);
        material.emission = glm::vec3(0.0f, 0.0f, 0.0f);
        material.phase_g = 0.0f;
        scene.materials.push_back(material);
    }

    // Set up light sources
    const hlcr::PointLight point_light0 = {glm::vec3(8.0f, 4.0f, 0.0f),
                                           glm::vec3(1.0f, 1.0f, 1.0f)};
    scene.point_lights.push_back(point_light0);

    const hlcr::PointLight point_light1 = {glm::vec3(0.0f, 8.0f, -8.0f),
                                           glm::vec3(1.0f, 1.0f, 1.0f)};
    scene.point_lights.push_back(point_light1);

    scene.sky_light.type = hlcr::SKY_LIGHT_TYPE_SOLID;
    scene.sky_light.intensity = glm::vec3(0.0f, 0.0f, 0.0f);
}

void initialize_disney_cloud_scene(hlcr::Scene &scene)
{
    // Create a large sphere representing the ground
    hlcr::Sphere sphere{};
    sphere.center = glm::vec3(0.0f, -5000.0f, 0.0f);
    sphere.radius = 5000.0f;
    sphere.material_id = 1;
    scene.spheres.push_back(sphere);

    // Load Disney cloud volume
    {
        const std::string filename = "./data/wdas_cloud_eighth_uint8.vtk";
        hlcr::VoxelBuffer voxel_buffer{};
        glm::vec3 spacing{};
        if (!hlcr::load_uint8_vtk_volume(filename, voxel_buffer.voxels, voxel_buffer.dimensions,
                                         spacing)) {
            assert(false && "could not load VTK volume");
        }

        LOG_INFO("sx: %f\n", spacing[0]);
        LOG_INFO("sy: %f\n", spacing[1]);
        LOG_INFO("sz: %f\n", spacing[2]);
        LOG_INFO("dx: %d\n", voxel_buffer.dimensions[0]);
        LOG_INFO("dy: %d\n", voxel_buffer.dimensions[1]);
        LOG_INFO("dz: %d\n", voxel_buffer.dimensions[2]);

        scene.voxel_buffers.push_back(voxel_buffer);
    }

    // Create volume instances
    {
        hlcr::Volume volume{};
        volume.type = hlcr::VOLUME_TYPE_VOXELS;
        volume.material_id = 0;
        volume.voxel_buffer_id = 0;
        volume.density = 1.0f;
        const glm::vec3 volume_extent_m =
            0.016f * glm::vec3(scene.voxel_buffers[volume.voxel_buffer_id].dimensions);
        volume.world_from_local =
            glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.6f * volume_extent_m[1], 0.0f)) *
            glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::scale(glm::mat4(1.0f), volume_extent_m) *
            glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.5f, -0.5f));
        volume.local_from_world = glm::inverse(volume.world_from_local);
        scene.volumes.push_back(volume);
    }

    // Create materials
    {
        hlcr::Material material{};
        material.type = hlcr::MATERIAL_TYPE_VOLUME;
        material.base_color = glm::vec3(0.0f, 0.0f, 0.0f);
        material.scattering = glm::vec3(1.0f, 1.0f, 1.0f);
        material.absorption = glm::vec3(2.0f, 6.0f, 4.0f);
        material.emission = glm::vec3(0.0f, 0.0f, 0.0f);
        material.phase_g = 0.5f;
        scene.materials.push_back(material);
    }

    {
        hlcr::Material material{};
        material.type = hlcr::MATERIAL_TYPE_SOLID;
        material.base_color = glm::vec3(0.2f, 0.2f, 0.2f);
        material.scattering = glm::vec3(0.0f, 0.0f, 0.0f);
        material.absorption = glm::vec3(0.0f, 0.0f, 0.0f);
        material.emission = glm::vec3(0.0f, 0.0f, 0.0f);
        material.phase_g = 0.0f;
        scene.materials.push_back(material);
    }

    // Set up light sources
    const hlcr::PointLight point_light0 = {glm::vec3(6.0f, 6.0f, -3.0f),
                                           0.5f * glm::vec3(1.5f, 1.5f, 1.5f)};
    scene.point_lights.push_back(point_light0);

#if 0
    scene.sky_light.type = hlcr::SKY_LIGHT_TYPE_SOLID;
    scene.sky_light.intensity = glm::vec3(0.0f, 0.0f, 0.0f);
#else
    const glm::vec3 sun_direction = glm::vec3(0.0f, -0.5f, -1.0f);
    const glm::vec3 sun_intensity = 0.02f * glm::vec3(1.0f, 1.0f, 1.0f);
    const hlcr::SkyParams sky_params = hlcr::get_default_sky_params(sun_direction, sun_intensity);
    hlcr::PixelBuffer envmap{};
    envmap.width = 200;
    envmap.height = 100;
    envmap.num_channels = 4;
    hlcr::create_rgba32f_sky_envmap(sky_params, 1.0f, envmap.width, envmap.height, 1, 30, 10, envmap.pixels);

    hlcr::write_ppm("sky2.ppm", envmap.pixels, envmap.width, envmap.height, envmap.num_channels);

    scene.sky_light.type = hlcr::SKY_LIGHT_TYPE_ENVMAP;
    scene.sky_light.envmap = envmap;
    scene.sky_light.intensity = glm::vec3(1.0f, 1.0f, 1.0f);
#endif
}

void initialize_scene(hlcr::Scene &scene, const SceneID scene_id)
{
    if (scene_id == SCENE_ID_BUNNIES) {
        initialize_bunnies_scene(scene);
    }
    else if (scene_id == SCENE_ID_DISNEY_CLOUD) {
        initialize_disney_cloud_scene(scene);
    }
    else {
        assert(false && "Unknown scene");
    }
}

// FIXME: this only works when the scene consists of non-overlapping cubic volumes of the same
// size. Should calculate the closest view space Z distance to the bounding boxes instead to make
// the sorting more robust.
void sort_volumes_back_to_front(const hlcr::Camera &camera, std::vector<hlcr::Volume> &volumes)
{
    assert(!volumes.empty());

    // Calculate view space Z distance to volume centers
    const glm::mat4 view_from_world = glm::lookAt(camera.eye, camera.center, camera.up);
    std::vector<float> distances;
    distances.reserve(volumes.size());
    for (const auto &volume : volumes) {
        const glm::vec4 world_pos = volume.world_from_local * glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
        const glm::vec4 view_pos = view_from_world * world_pos;
        distances.push_back(std::abs(view_pos[2]));
    }

    // Sort the volumes in back-to-front order
    for (uint32_t i = volumes.size() - 1; i > 0; --i) {
        for (uint32_t j = 0; j < i; ++j) {
            if (distances[j] < distances[j + 1]) {
                const float distance_tmp = distances[j + 1];
                distances[j + 1] = distances[j];
                distances[j] = distance_tmp;

                const hlcr::Volume volume_tmp = volumes[j + 1];
                volumes[j + 1] = volumes[j];
                volumes[j] = volume_tmp;
            }
        }
    }
}

glm::vec3 trace_ray(hlcr::Ray &world_ray, const hlcr::Scene &scene, const Settings &settings,
                    hlcr::RNGState &rng_state, const float jitter)
{
    glm::vec3 color = {0.0f, 0.0f, 0.0f};
    if (settings.render_mode == RENDER_MODE_RAYMARCHING) {
        color = hlcr::trace_ray_rm(world_ray, scene, settings.step_size_m,
                                   settings.visibility_step_size_m, jitter);
    }
    else if (settings.render_mode == RENDER_MODE_DELTA_TRACKING) {
        color = hlcr::trace_ray_dt(world_ray, scene, rng_state);
    }
    else {
        assert(false && "Unsupported rendering mode.");
    }

    return color;
}

int main()
{
    Settings settings;

    // Note: the raymarching backend is really slow compared to the delta tracking backend (since
    // the raymarcher casts a visibility ray for every step), but produces an almost noise-free
    // result. So we limit the SPP to 1 for raymarching while allowing more SPP for delta tracking.
    settings.spp = settings.render_mode == RENDER_MODE_RAYMARCHING ? 1 : settings.spp;

    const uint32_t num_channels = 3;
    std::vector<float> image(settings.width * settings.height * num_channels, 0.0f);

    // Initialize scene
    hlcr::Camera camera{};
    camera.eye = glm::vec3(0.0f, 3.5f, 8.0f);
    camera.center = glm::vec3(0.0f, 1.0f, 0.0f);
    camera.up = glm::vec3(0.0f, 1.0f, 0.0f);
    camera.fovy = glm::radians(35.0f);
    camera.aspect = float(settings.width) / settings.height;

    hlcr::Scene scene{};
    double tic = omp_get_wtime();
    initialize_scene(scene, settings.scene_id);
    sort_volumes_back_to_front(camera, scene.volumes);
    double toc = omp_get_wtime();
    LOG_INFO("Scene initialization: %f s\n", toc - tic);

    // Render
    tic = omp_get_wtime();
#pragma omp parallel for schedule(dynamic, 1)
    for (uint32_t j = 0; j < settings.height; ++j) {
        for (uint32_t i = 0; i < settings.width; ++i) {
            const glm::vec2 uv = {float(i) / settings.width, 1.0f - float(j) / settings.height};

            hlcr::RNGState rng_state = {hlcr::wang_hash(i + j * settings.width)};

            glm::vec3 color = {0.0f, 0.0f, 0.0f};
            for (uint32_t k = 1; k <= settings.spp; ++k) {
                const glm::vec2 aa_offset =
                    (hlcr::ldseq_r2(k) - 0.5f) / glm::vec2(settings.width, settings.height);

                hlcr::Ray world_ray{};
                hlcr::generate_ray(camera, uv + aa_offset, world_ray);
                world_ray.t_min = 0.0f;
                world_ray.t_max = 1e6f;

                const float jitter = std::fmod(hlcr::hash2d(uv[0], uv[1]) + 1.61803f * k, 1.0f);

                color += trace_ray(world_ray, scene, settings, rng_state, jitter);
            }
            color /= float(settings.spp);

            color *= settings.exposure;
            color = hlcr::tonemap_aces(color);
            color = hlcr::lin2srgb(color);
            color = glm::clamp(color, 0.0f, 1.0f);

            const uint32_t pixel_index = (i + j * settings.width) * num_channels;
            image[pixel_index + 0] = color[0];
            image[pixel_index + 1] = color[1];
            image[pixel_index + 2] = color[2];
        }
    }
    toc = omp_get_wtime();
    LOG_INFO("Raymarching: %f s\n", toc - tic);

    // Export image
    hlcr::write_ppm("output.ppm", image, settings.width, settings.height, num_channels);
    hlcr::write_pfm("output.pfm", image, settings.width, settings.height, num_channels);

    return 0;
}
