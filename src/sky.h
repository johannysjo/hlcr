/// @file
/// @brief Procedural HDR sky environment map generator that simulates atmospheric scattering with
/// raymarching.
///
/// @section LICENSE
///
/// Copyright (c) 2020 Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace hlcr {

struct SkyParams {
    glm::vec3 earth_center;
    float earth_radius_km;
    float atmosphere_radius_km;
    glm::vec3 mie_scattering;
    glm::vec3 rayleigh_scattering;
    float mie_scale_height_km;
    float rayleigh_scale_height_km;
    float mie_phase_g;
    glm::vec3 sun_direction;
    glm::vec3 sun_intensity;
};

SkyParams get_default_sky_params(const glm::vec3 &sun_direction, const glm::vec3 &sun_intensity);

void create_rgba32f_sky_envmap(const SkyParams &sky_params, float altitude_km, uint32_t width,
                               uint32_t height, uint32_t spp, uint32_t num_primary_steps,
                               uint32_t num_secondary_steps, std::vector<float> &pixel_data);

} // namespace hlcr
