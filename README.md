# HLCR - Happy Little Cloud Renderer

HLCR is a CPU-based volume renderer that can be used for rendering voxelized meshes (or other volumetric models) as happy little clouds :) Imported models are "cloudified" by offsetting the voxel coordinates with 3D fractional Brownian motion (fBm noise). The idea was to implement both a traditional ray marching backend and a more modern delta tracking-based backend with progressive refinement, but currently the renderer only supports ray marching. Work in progress...

## Example screenshot

![Screenshot](https://bitbucket.org/johannysjo/hlcr/raw/master/screenshot.png "Screenshot")

## Compiling (Linux)

Clone the git repository and run (from the root folder)
```bash
$ ./build.sh
```

Requires CMake 3.0 or higher and a C++11 capable compiler (GCC 4.6+).

## License

The source code is provided under the MIT license. See LICENSE.txt for more information.
