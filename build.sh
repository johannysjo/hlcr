#!/bin/bash

export HLCR_ROOT=$(pwd)

cd $HLCR_ROOT && \
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -DCMAKE_INSTALL_PREFIX=$HLCR_ROOT ../ && \

make -j6 && \
make install && \

cd .. && \
bin/hlcr && \
display output.ppm
